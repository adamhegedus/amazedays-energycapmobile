﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Plugin.Media;
using System.Net.Http;
using System.Diagnostics;
using System.IO;
using Xamarin.Auth;
using System.Linq;
using System.Net.Http.Headers;

namespace EnergyCAPMobile
{
    public partial class LoggedInHomePage : ContentPage
    {

        const string ServiceId = "EnergyCAP";
        const string Scope = "profile";

        Account account;

        public LoggedInHomePage()
        {
            InitializeComponent();
        }


        async void takePhotoButtonClicked(object sender, EventArgs eventArgs)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", "No camera available.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                SaveToAlbum = true
            });

            // wait until the file is written
            while (File.ReadAllBytes(file.Path).Length == 0)
            {
                System.Threading.Thread.Sleep(1);
            }

            if (file == null)
                return;

            //billImage.Source = ImageSource.FromStream(() =>
            //{
            //    var stream = file.GetStream();
            //    return stream;
            //});

            try
            {
                account = AccountStore.Create().FindAccountsForService(ServiceId).FirstOrDefault();

                //read file into upfilebytes array
                var upfilebytes = File.ReadAllBytes(file.Path);

                //create new HttpClient and MultipartFormDataContent and add our file, and StudentId
                HttpClient client = new HttpClient();
                MultipartFormDataContent content = new MultipartFormDataContent();
                ByteArrayContent baContent = new ByteArrayContent(upfilebytes);
                //StringContent studentIdContent = new StringContent("2123");
                content.Add(baContent, "Image", "filename.ext");
                //content.Add(studentIdContent, "StudentId");
                //baContent.Headers.Add("Authorization", "Bearer: " + account.Properties["access_token"]);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", account.Properties["access_token"]);

                //upload MultipartFormDataContent content async and store response in response var
                var response = await client.PostAsync("https://my-dev.energycap.com/api/v1/amaze/billimage", content);

                //read response result as a string async into json var
                var responsestr = response.Content.ReadAsStringAsync().Result;

                //debug
                Debug.WriteLine(responsestr);

            }
            catch (Exception e)
            {
                //debug
                Debug.WriteLine("Exception Caught: " + e.ToString());

                return;
            }

        }


        private void uploadPhotoButtonClicked(object sender, EventArgs e){
            
        }


        private async void UploadImage()
        {
            //variable
            var url = "https://my-dev.energycap.com/api/v1/amaze/billimage";
            var file = "path/to/file.ext";

            try
            {
                //read file into upfilebytes array
                var upfilebytes = File.ReadAllBytes(file);

                //create new HttpClient and MultipartFormDataContent and add our file, and StudentId
                HttpClient client = new HttpClient();
                MultipartFormDataContent content = new MultipartFormDataContent();
                ByteArrayContent baContent = new ByteArrayContent(upfilebytes);
                StringContent studentIdContent = new StringContent("2123");
                content.Add(baContent, "File", "filename.ext");
                content.Add(studentIdContent, "StudentId");


                //upload MultipartFormDataContent content async and store response in response var
                var response =
                    await client.PostAsync(url, content);

                //read response result as a string async into json var
                var responsestr = response.Content.ReadAsStringAsync().Result;

                //debug
                Debug.WriteLine(responsestr);

            }
            catch (Exception e)
            {
                //debug
                Debug.WriteLine("Exception Caught: " + e.ToString());

                return;
            }
        }
    }
}
