﻿using System;
namespace EnergyCAPMobile
{
    public class ServerInfo
    {
        public static Uri AuthorizationEndpoint = new Uri("https://auth-dev.energycap.com/auth/realms/myenergycap/protocol/openid-connect/auth");
        public static Uri TokenEndpoint = new Uri("https://auth-dev.energycap.com/auth/realms/myenergycap/protocol/openid-connect/token");
        public static Uri ApiEndpoint = new Uri("https://auth-dev.energycap.com/auth/realms/myenergycap/protocol/openid-connect/token");
        public static Uri RedirectionEndpoint = new Uri("energycapmobile:/oauth_callback");
        public static string ClientId = "https://advhosting.energycap.com";
        public static string ClientSecret = "0e77d88b-a3eb-4fec-96bf-4e60f5f96ae2";
    }
}
