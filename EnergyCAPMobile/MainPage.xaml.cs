﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Xamarin.Auth;

namespace EnergyCAPMobile
{
    public partial class MainPage : ContentPage
    {
        const string ServiceId = "EnergyCAP";
        const string Scope = "profile";

        Account account;


        public MainPage()
        {
            InitializeComponent();

            account = AccountStore.Create().FindAccountsForService(ServiceId).FirstOrDefault();

            if (account != null)
            {
                this.refreshToken();
            }
        }


        void loginCLicked(object sender, EventArgs e)
        {
            OAuth2Authenticator authenticator = new OAuth2Authenticator
                (
                    clientId: ServerInfo.ClientId,
                    clientSecret: ServerInfo.ClientSecret,
                    scope: "",
                    authorizeUrl: ServerInfo.AuthorizationEndpoint,
                    redirectUrl: ServerInfo.RedirectionEndpoint,
                    accessTokenUrl: ServerInfo.TokenEndpoint,
                    isUsingNativeUI: true
                );

            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;

            AuthenticationState.Authenticator = authenticator;

            this.PresentUILoginScreen(authenticator);
        }


        private void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            throw new NotImplementedException();
        }


        private void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;

            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            if (e.IsAuthenticated)
            {
                getProfileButton.IsEnabled = true;

                //if (this.account != null)
                    //AccountStore.Delete(this.account, ServiceId);

                AccountStore.Create().Save(account = e.Account, ServiceId);

                getProfileButton.IsEnabled = true;
                loginButton.IsEnabled = false;
                logoutButton.IsEnabled = true;

                if (account.Properties.ContainsKey("expires_in"))
                {
                    var expires = int.Parse(account.Properties["expires_in"]);
                    loginResponse.Text = "Token lifetime is: " + expires + "s";
                }
                else
                {
                    loginResponse.Text = "Authentication succeeded";
                }

                if (account.Properties.ContainsKey("refresh_token"))
                    refreshButton.IsEnabled = true;
            }
            else
            {
                loginResponse.Text = "Authentication failed";
            }
        }


        async void refreshButtonClicked(object sender, EventArgs eventArgs)
        {
            this.refreshToken();
        }


        async void getProfileButtonClicked(object sender, EventArgs eventArgs)
        {
            throw new NotImplementedException();
        }


        async void logoutClicked(object sender, EventArgs eventArgs)
        {
            AccountStore.Create().Delete(account, ServiceId);

            loginButton.IsEnabled = true;
            refreshButton.IsEnabled = false;
            getProfileButton.IsEnabled = false;
            logoutButton.IsEnabled = false;
        }


        async void refreshToken() {
            var myRefreshToken = account.Properties["refresh_token"];

            if (string.IsNullOrWhiteSpace(myRefreshToken))
                return;

            var queryValues = new Dictionary<string, string>
                {
                    {"refresh_token", myRefreshToken},
                    {"client_id", ServerInfo.ClientId},
                    {"grant_type", "refresh_token"},
                    {"client_secret", ServerInfo.ClientSecret},
                };

            var authenticator = new OAuth2Authenticator
                (
                    ServerInfo.ClientId,
                    ServerInfo.ClientSecret,
                    "profile",
                    ServerInfo.AuthorizationEndpoint,
                    ServerInfo.RedirectionEndpoint,
                    ServerInfo.TokenEndpoint,
                    null,
                    isUsingNativeUI: true
                );

            try
            {
                var result = await authenticator.RequestAccessTokenAsync(queryValues);

                if (result.ContainsKey("access_token"))
                    account.Properties["access_token"] = result["access_token"];

                if (result.ContainsKey("refresh_token"))
                    account.Properties["refresh_token"] = result["refresh_token"];

                AccountStore.Create().Save(account, ServiceId);

                loginButton.IsEnabled = false;
                refreshButton.IsEnabled = true;
                getProfileButton.IsEnabled = true;
                logoutButton.IsEnabled = true;

                loginResponse.Text = "Refresh succeeded";

                Navigation.PushAsync(new LoggedInHomePage());
            }
            catch (Exception ex)
            {
                loginButton.IsEnabled = true;
                refreshButton.IsEnabled = false;
                getProfileButton.IsEnabled = false;

                loginResponse.Text = "Refresh failed " + ex.Message;
            }

            return;
        }
    }

   
}
