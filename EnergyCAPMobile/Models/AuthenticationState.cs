﻿using System;

using Xamarin.Auth;

namespace EnergyCAPMobile
{
    public class AuthenticationState
    {
        /// <summary>
        /// The authenticator.
        /// </summary>
        // TODO:
        // Oauth1Authenticator inherits from WebAuthenticator
        // Oauth2Authenticator inherits from WebRedirectAuthenticator
        public static OAuth2Authenticator Authenticator;
    }
}